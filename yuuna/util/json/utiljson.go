// Json utilities
package json

import (
	"fmt"
	"os"
	"encoding/json"

	// XXX use ordered json ?
	// jsonmap/metalim ?
	"github.com/mitranim/jsonfmt"
)

// Format json
func JsonFormat(inBs []byte) []byte {
	formatted := jsonfmt.FormatBytes(jsonfmt.Default, inBs)
	
	return formatted
}

func JsonUnmarshalJsonFromFile(path string) map[string]interface{} {

	content, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}

	//pp(out)

	// XXX
	xcontent := RemoveComments(string(content))
	
	//outB := []byte(content)
	outB := []byte(xcontent)

	var dat map[string]interface{}

	// Decode and a check for errors.
	if err := json.Unmarshal(outB, &dat); err != nil {
		panic(err)
	}
	//fmt.Println(dat)

	return dat
}

/*
func _getObject2(
	data map[string]interface{}, name string) map[string]interface{} {
	return data[name].(map[string]interface{})
}
*/
// Convert json5 data to json5 object.
func HjGetObject(data interface{}) map[string]interface{} {
	return data.(map[string]interface{})
}

func HjGetObjectCheck(data interface{}) (map[string]interface{}, bool) {
	if data == nil {
		return nil, false
	}
	return data.(map[string]interface{}), true
}

// Get string value from json5 def.
func HjGetString(def map[string]interface{}, name string) string {
	if v := def[name]; v != nil {
		return v.(string)
	} else {
		panic("read fail key=" + name)
	}
}

// Get float value from json5 def.
func HjGetFloat(def map[string]interface{}, name string) float64 {
	if v := def[name]; v != nil {

		return v.(float64)
	} else {
		panic("read fail key=" + name)
	}
}

// Get int value from json5 def.
func HjGetInt(def map[string]interface{}, name string) int {
	vf := HjGetFloat(def, name)
	// Check if value is integer
	if vf != float64(int(vf)) {
		panic(fmt.Sprintf("error: json value is not integer: %v", vf))
	}
	
	return int(vf)
}

func HjHasDef(def map[string]interface{}, name string) bool {
	return def[name] != nil
}

func HjGetDef(def map[string]interface{}, name string) map[string]interface{} {
	x := def[name]
	if x != nil {
		return x.(map[string]interface{})
	} else {
		panic("get def fail key=" + name)
	}
}

func HjGetBool(def map[string]interface{}, name string) bool {
	if v := def[name]; v != nil {
		return v.(bool)
	} else {
		panic("read fail key=" + name)
	}
}

func HjGetStringDefault(def map[string]interface{}, name string, defaultVal string) string {
  if HjHasDef(def, name) {
    return HjGetString(def, name)
  }
  return defaultVal
}

func HjGetFloatDefault(def map[string]interface{}, name string, defaultVal float64) float64 {
  if HjHasDef(def, name) {
    return HjGetFloat(def, name)
  }
  return defaultVal
}

func HjGetIntDefault(def map[string]interface{}, name string, defaultVal int) int {
  if HjHasDef(def, name) {
    return HjGetInt(def, name)
  }
  return defaultVal
}
