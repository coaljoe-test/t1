ship structure:

simple:

 -base
 |
 -turret (front)
 -turret (rear)
 

test:
 
 base
 |-- turret 1 (front)
 |-- turret 2 (rear)
 |-- turret 3

flat graph:

 (base) -> (turret 1)   -- At the front
        -> (turret 2)   -- At the back 1
        -> (turret 3)   -- At the back 2

 (base) -> (turret 1)
        -> (turret 2)
        -> (turret 3)
           -> (sub turret ?)


   [_]      [_]
    __\__ _/___
    \_________/


   [__]      [__] [__]
     \        /    /
    ________________
    \______________/


    
 (ship_base) -> (front_turret)
             -> (back_turret 1)
             -> (back_turret 2)


                       (?)
                      [__]
                     /
   [__]      [__] [__]
     \        /    /
    ________________
    \______________/

 (ship_base) -> (front_turret)
             -> (back_turret_1)
             -> (back_turret_2)
                ? -> (small_rocket_turret)



- ? composite turrets
- ? composite weapons
- ? stacked turret
