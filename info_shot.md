? shot kinds:

  - ? projectile
  - ? shot
  - ? rocket
    - ? guided / unguided


? ream type:

  - surface to air
  - air to air
  - air to surface
  - ? surface to surface

  - ? surface to both surface and air ?


(realm)

| shot_from | shot_to | name           | info
|---        | ---     | ---            | ---
| surface   | air     | surface to air | |
| air       | surface | air to surface | |
| surface   | surface | surface to surface | |
| air       | air     | air to air | |

(mask ?)

| surface_mask | air_mask | name | info
|---           | ---      | ---  | ---
| x            |          | surface to air | |
|              | x        | air to surface | |
| x            | x        | surface to surface | |


| source_surface_mask | source_air_mask | target_surface_mask | target_air_mask | name | info
| ---                 | ---             | ---                 | ---             | ---  | ---
| x                   |                 |                     |                 | surface to air |
|                     | x               |                     |                 | air to surface  | 
| x                   | x               |                     |                 | surface to surface |
| x                   | x               |                     |                 | surface to surface |
| x                   | x               | x                   | x               | ? all (?) | 

(test)

(from - > to)

x = set

| source_surface_mask | source_air_mask | target_surface_mask | target_air_mask | name | info
| ---                 | ---             | ---                 | ---             | ---  | ---
| x                   |                 |                     | x               | (surface) surface to air |
| x                   |                 | x                   |                 | (surface) surface to surface | 
| x                   |                 | x                   | x               | (surface) surface to (air and surface) (both ?) (all ?) | 
|                     | x               |                     | x               | (air) air to air |
|                     | x               | x                   |                 | (air) air to surface  |
|                     | x               | x                   | x               | (air) air to all (?)  |
| x                   | x               |                     | x               | (all ?) ? to air |
| x                   | x               | x                   |                 | (all ?) ? to surface  |
| x                   | x               | x                   | x               | (all ?) ? to all (?)  | 
|                     |                 |                     |                 | ? not specified (?) (all ?)  | 
| x                   | x               | x                   | x               | ? all (?) | 

---

* separate source/target realms ?
  * ? use two realms (separate realm for source and from target)